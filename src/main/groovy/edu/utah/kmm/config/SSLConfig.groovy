package edu.utah.kmm.config

class SSLConfig {
	private ConfigObject sslConfig
	
	public SSLConfig(String sslConfigLocation){
		this.sslConfig = new ConfigSlurper().parse(new File(sslConfigLocation).text)
	}

	public Boolean isEnable(){
		return sslConfig.sslEnable
	}
		
	public String getPass() {
		return sslConfig.sslPassword
	}
}
