package edu.utah.kmm.config

class Config {
	def iss
	def launch
	def clientId
	def scope
	def redirectUrl
	def launchUrl
	def accessToken
	def patientId
	def code
	def fhirResourcesPath
}