yieldUnescaped '<!DOCTYPE html>'
html(lang:'en'){
  head {
    meta(charset:'utf-8')
    meta('http-equiv': 'X-UA-Compatible', content: 'IE=edge')
	meta('http-equiv': 'Pragma', content: 'no-cache')
	meta('http-equiv': 'Expirers', content: '-1')
    meta(name: 'viewport', content: 'width=device-width, initial-scale=1')
    title("$title")	
    link(href:'images/favicon.ico', rel: 'icon', type:'image/x-icon' )
	link('rel':'stylesheet', 'type':'text/css', 'href':'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css')
  }
  body {
	 header ('class':'navbar navbar-inverse navbar-static-top', 'role':'navigation', style:'background-color: #4D5B66') {
	 }
	 div (class:'alert alert-danger') {
		 h2 errorTitle
		 h5 msg
	 }
  }
}