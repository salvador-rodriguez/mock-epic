yieldUnescaped '<!DOCTYPE html>'
html {
  head {
    meta(charset:'utf-8')
    title("Ratpack: $title")

    meta(name: 'apple-mobile-web-app-title', content: 'Ratpack')
    meta(name: 'description', content: '')
    meta(name: 'viewport', content: 'width=device-width, initial-scale=1')

    link(href: '/images/favicon.ico', rel: 'shortcut icon')
    link('rel':'stylesheet', 'type':'text/css', 'href':'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css')
  }
  body {

    div(id:"main", class:"container-fluid") {
      h1 'Mock Epic'
      p 'OAuth2 and FHIR'

      form(method: "get", action: "${launchUrl}") {
        input( type:'hidden', name:'iss', value:"${iss}")
        input( type:'hidden', name:'launch', value:"${launch}")
        input(type: "submit", class: "btn btn-success", value: "Launch App!")
      }
    }
  }
}
