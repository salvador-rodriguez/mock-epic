import cors.CORSHandler
import edu.utah.kmm.config.Config
import edu.utah.kmm.config.SSLConfig
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import ratpack.form.Form
import ratpack.groovy.template.MarkupTemplateModule
import ratpack.handling.RequestLogger
import ratpack.http.Request
import ratpack.server.internal.DefaultServerConfigBuilder
import ratpack.ssl.SSLContexts
import static ratpack.jackson.Jackson.json

import java.nio.file.Paths

import static ratpack.groovy.Groovy.groovyMarkupTemplate
import static ratpack.groovy.Groovy.ratpack

ratpack {
    final Logger log = LoggerFactory.getLogger(Ratpack.class)
    def dotMockEpicServer = "${System.getProperty('user.home')}/.mock-epic-server"

    serverConfig { DefaultServerConfigBuilder configBuilder ->
        port 5051
        configBuilder.require("/config", Config)
        configBuilder.json(Paths.get("${dotMockEpicServer}/config.json"))
        SSLConfig ssl = new SSLConfig("${dotMockEpicServer}/sslConfig.properties")
        if (ssl.isEnable()) {
            configBuilder.ssl(SSLContexts.sslContext(Paths.get("${dotMockEpicServer}/hostCert.jks"), ssl.pass))
        }
    }

    bindings {
        module(MarkupTemplateModule) { cnf ->
            cnf.autoEscape = false
        }
    }

    handlers {

        def state

        all RequestLogger.ncsa(log)

        all (new CORSHandler())

        get() { Config config ->
            render groovyMarkupTemplate("index.gtpl",
                    launchUrl: config.launchUrl,
                    iss: config.iss,
                    launch: config.launch)
        }

        get("metadata") { Config config, Request request ->
            def fhirResPath = config.fhirResourcesPath
            render new File("$fhirResPath/conformance.json").text
        }

        get("authorize") { Config config ->
            def params = request.queryParams
            println params

            def launch = params?.launch
            def redirectUri = params?.redirect_uri
            def clientId = params?.client_id
            def responseType = params?.response_type
            def scope = params?.scope
            state = params?.state

            if (!redirectUri.equals(config.redirectUrl)) {
                log.error("wrong redirect url $redirectUri")
                render groovyMarkupTemplate("error.gtpl", errorTitle:"GET authorize request, wrong redirect_url",
                        msg:"received value : ${redirectUri} <br>expected value : ${config.redirectUrl}")
            }
            if (!clientId.equals(config.clientId)) {
                log.error("wrong client id $clientId")
                render groovyMarkupTemplate("error.gtpl", errorTitle:"GET authorize request, wrong client_id",
                        msg:"received value : ${clientId} <br>expected value : ${config.clientId}")
            }
            if (!scope.equals(config.scope)) {
                log.error("wrong scope $scope")
                render groovyMarkupTemplate("error.gtpl", errorTitle:"GET authorize request, wrong scope",
                        msg:"received value : ${scope} <br>expected value : ${config.scope}")
            }

            render groovyMarkupTemplate("redirect.gtpl",
                    url: "${config.redirectUrl}?code=${config.code}&state=${state}")

        }

        post("token") { Config config ->
            parse(Form.class).then { f ->
                def code = f.code
                def redirectUri = f.redirect_uri
                def clientId = f.client_id

                if (!redirectUri.equals(config.redirectUrl)) {
                    log.error("wrong redirect url $redirectUri")
                    render groovyMarkupTemplate("error.gtpl", errorTitle:"GET token request, wrong redirect_url",
                            msg:"received value : ${redirectUri} <br>expected value : ${config.redirectUrl}")
                }
                if (!clientId.equals(config.clientId)) {
                    log.error("wrong client id $clientId")
                    render groovyMarkupTemplate("error.gtpl", errorTitle:"GET token request, wrong client_id",
                            msg:"received value : ${clientId} <br>expected value : ${config.clientId}")
                }
                if (!code.equals(config.code)) {
                    log.error("wrong code $code")
                    render groovyMarkupTemplate("error.gtpl", errorTitle:"GET token request, wrong code",
                            msg:"received value : ${code} <br>expected value : ${config.code}")
                }

                render json(access_token: config.accessToken, patient: config.patientId)
            }
        }

        files { dir "static" }

        /**
         * FHIR resources
         */

        get("Patient/:id?") { Config config ->
            def patId = pathTokens.id
            log.debug "patientId: ${patId}"
            def params = request.queryParams
            log.debug "params: ${params}"
            render new File("$config.fhirResourcesPath/patient.json").text
        }

        get ("Observation") { Config config ->
            def params = request.queryParams
            log.debug "params: ${params}"
            render new File("$config.fhirResourcesPath/observations.json").text
        }

        get ("MedicationStatement") { Config config ->
            def params = request.queryParams
            log.debug "params: ${params}"
            render new File("$config.fhirResourcesPath/medicationStatements.json").text
        }

        /**
         *
         */
    }
}
