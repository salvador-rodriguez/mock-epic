Mock Epic server
-----------------------------

### Setup mock-server folder

Copy the .mock-server folder located inside of test/resources and put it in your HOME directory.  
The content will look like this:
    
    .mock-epic-server/
    ├── FHIR_resources
    │   ├── conformance.xml
    │   ├── observations.json
    │   └── patient.json
    ├── config.json
    ├── hostCert.jks
    └── sslConfig.properties

Define the SMART on FHIR parameters in config.json

### Run server

You can run the server in debug mode with the following command.  The server will be available in port 5051

    ./gradlew -t run

### Launch application

Open your browser on https://localhost:5051/ and click in "Launch App!" in order to launch your SMART on FHIR application.
